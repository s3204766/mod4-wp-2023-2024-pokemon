package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.Pokemon;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

import java.util.ArrayList;
import java.util.List;

@Path("/pokemon")
public class PokemonRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<Pokemon> getPokemon(){
        List<Pokemon> pokemonList = new ArrayList<Pokemon>();
        pokemonList.add(new Pokemon("dhskdjhagkj", "1", "123", "123", 1,2,3,4,5,6,7,8));
        pokemonList.add(new Pokemon("fjsjhguk", "2", "123", "123",2,3,4,5,6,7,8,9));
        return new ResourceCollection<>(pokemonList.toArray(new Pokemon[0]), pokemonList.size(), 1, pokemonList.size());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pokemon getPokemon(@PathParam("id") String id) {
        List<Pokemon> pokemonList = new ArrayList<Pokemon>();
        pokemonList.add(new Pokemon("dhskdjhagkj", "1", "123", "123", 1,2,3,4,5,6,7,8));
        pokemonList.add(new Pokemon("fjsjhguk", "2", "123", "123",2,3,4,5,6,7,8,9));
        for(Pokemon x : pokemonList){
            if(x.id.equals(id)){
                return x;
            }
        }
        return null;
    }
}
