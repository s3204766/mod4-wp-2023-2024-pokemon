package nl.utwente.mod4.pokemon;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import nl.utwente.mod4.pokemon.model.PokemonType;

@ApplicationPath("/api")
public class PokeApp extends Application {
}