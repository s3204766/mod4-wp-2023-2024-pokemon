package nl.utwente.mod4.pokemon.model;

public class Pokemon extends NamedEntity{
    float height;
    float weight;
    int hp;
    int attack;
    int spAttack;
    int defense;
    int spDefense;
    int speed;

    public Pokemon(String name, String id, String created, String lastUpdated, float height, float weight, int hp, int attack, int spAttack, int defense, int spDefense, int speed) {
        this.name = name;
        this.id = id;
        this.created = created;
        this.lastUpDate = lastUpdated;
        this.height = height;
        this.weight = weight;
        this.hp = hp;
        this.attack = attack;
        this.spAttack = spAttack;
        this.defense = defense;
        this.spDefense = spDefense;
        this.speed = speed;
    }
}
